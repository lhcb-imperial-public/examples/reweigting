###############################################################################
#                                                                             #
# There can be many situations in which you wish to reweight data. For        #
# example, if there is a small mismodeling between data and simulation, or if #
# you want to change the kinematics of one mode to look like another. This    #
# example will go through some of the techniques that can be used to reweight #
# some 2D distributions using simple histograms, or machine learning.         #
#                                                                             #
###############################################################################



from ROOT import TFile, TCanvas

###############################################################################
#                                                                             #
#                           Setting up some data                              #
#                                                                             #
###############################################################################


# We have some example datasets in the data directory, let's get those out

infile = TFile('data/example.root')
datatree =  infile.Get('data')
mctree =  infile.Get('MC')

# These are example sets that we might find in the data and in the Monte Carlo simulation.
# Here the PT and Eta distributions for the B meson between the data and MC are slightly
# off. The rest of the event is correct for a given (B PT, B eta) value, but the population
# in (B PT, B eta) space isn't quite right. We might not directly care about these values,
# but it can impact things we might care about, e.g. the PT distribution of the decay
# products.

from ROOT import TH1D, TH2D, kBlack, kRed, kBlue, kGreen, kMagenta

print('''Building the data histograms''')
hbpt_data = TH1D('',';B_{PT}', 100, 0, 20)
hbeta_data = TH1D('',';B #eta', 100, 0, 8)
hbptveta_data = TH2D('',';B_{PT};B #eta', 10, 0, 20, 10, 0, 8)
hkstpt_data = TH1D('',';K^{*0}_{PT}', 100, 0, 10)
hksteta_data = TH1D('',';K^{*0} #eta', 100, -2, 5)

for ievent in range(datatree.GetEntries()):
  datatree.GetEntry(ievent)
  hbpt_data.Fill(datatree.B_PT)
  hbeta_data.Fill(datatree.B_ETA)
  hbptveta_data.Fill(datatree.B_PT, datatree.B_ETA)
  hkstpt_data.Fill(datatree.Kst_PT)
  hksteta_data.Fill(datatree.Kst_ETA)

for h in [hbpt_data, hbeta_data, hkstpt_data, hksteta_data]:
  h.SetLineColor(kBlack)
  h.SetLineWidth(2)


# Make the equivalent histograms for the MC
print('''Building the MC histograms''')
hbpt_mc = TH1D('',';B_{PT}', 100, 0, 20)
hbeta_mc = TH1D('',';B #eta', 100, 0, 8)
hbptveta_mc = TH2D('',';B_{PT};B #eta', 10, 0, 20, 10, 0, 8)
hkstpt_mc = TH1D('',';K^{*0}_{PT}', 100, 0, 10)
hksteta_mc = TH1D('',';K^{*0} #eta', 100, -2, 5)
for ievent in range(mctree.GetEntries()):
  mctree.GetEntry(ievent)
  hbpt_mc.Fill(mctree.B_PT)
  hbeta_mc.Fill(mctree.B_ETA)
  hbptveta_mc.Fill(mctree.B_PT, mctree.B_ETA)
  hkstpt_mc.Fill(mctree.Kst_PT)
  hksteta_mc.Fill(mctree.Kst_ETA)

for h in [hbpt_mc, hbeta_mc, hkstpt_mc, hksteta_mc]:
  h.SetLineColor(kRed)
  h.SetLineWidth(2)

def drawhists(hists):
  opts = ''
  for hist in sorted(hists, reverse=True, key=lambda x: x.GetMaximum()):
    print(opts)
    hist.Draw(opts)
    opts = 'same'

canvas_pt_mc = TCanvas()
drawhists([hbpt_data, hbpt_mc])

canvas_eta_mc = TCanvas()
drawhists([hbeta_data, hbeta_mc])

canvas_kstpt_mc = TCanvas()
drawhists([hkstpt_data, hkstpt_mc])


print('''There can often be small unwanted differences between the data and MC (or indeed any distributions you care about)''')
print('''Here we can see the distributions from two sources (black is data, red is MC), for the PT and Eta of the B mesons''')
input("Press Enter to continue...")


###############################################################################
#                                                                             #
#                          Using Simple histograms                            #
#                                                                             #
###############################################################################

# We have a 2D histogram for the data and MC distributions. A simple way to
# reweight the distributions is to take the ratio of these histograms

print('''''')
print('''        Reweighting with histograms''')
print('''''')

h_weights = hbptveta_data.Clone()
h_weights.Divide(hbptveta_mc)

# Apply these binned weights to the MC
print('''Building the MC histograms''')
hbw = TH1D('', ';Binned weight', 100, h_weights.GetMinimum(), h_weights.GetMaximum())
hbpt_bw = TH1D('',';B_{PT}', 100, 0, 20)
hbeta_bw = TH1D('',';B #eta', 100, 0, 8)
hbptveta_bw = TH2D('',';B_{PT};B #eta', 10, 0, 20, 10, 0, 8)
hkstpt_bw = TH1D('',';K^{*0}_{PT}', 100, 0, 10)
hksteta_bw = TH1D('',';K^{*0} #eta', 100, -2, 5)
for ievent in range(mctree.GetEntries()):
  mctree.GetEntry(ievent)
  w = h_weights.GetBinContent(h_weights.FindBin(mctree.B_PT, mctree.B_ETA))
  hbw.Fill(w)
  hbpt_bw.Fill(mctree.B_PT, w)
  hbeta_bw.Fill(mctree.B_ETA, w)
  hbptveta_bw.Fill(mctree.B_PT, mctree.B_ETA, w)
  hkstpt_bw.Fill(mctree.Kst_PT, w)
  hksteta_bw.Fill(mctree.Kst_ETA, w)

for h in [hbpt_bw, hbeta_bw, hkstpt_bw, hksteta_bw, hbw]:
  h.SetLineColor(kGreen)
  h.SetLineWidth(2)

canvas_pt_bw = TCanvas()
drawhists([hbpt_data, hbpt_bw])

canvas_eta_bw = TCanvas()
drawhists([hbeta_data, hbeta_bw])

canvas_kstpt_bw = TCanvas()
drawhists([hkstpt_data, hkstpt_bw])

canvas_bw = TCanvas()
hbw.Draw()

print('''Some plots after reweighting using histograms''')
input("Press Enter to continue...")


###############################################################################
#                                                                             #
#               Using the HEP ML algorithm to do the reweighting              #
#                                                                             #
###############################################################################

# Sometimes the uncertatines involved in making histograms become large, e.g:
#    if the data sets are small
#    if the distributions change quickly across a bin
#    if there are a large number of dimensions
# It always boils down to having sufficient stats in a bin to model the distributions.
# This can be rectified by having adaptive binning schemes, but they can also be
# addressed by use of machine learning. We will use the HEP ML reweighting package.
# ML methods need to be checked for stability, they can produce larger weights
# than seen when using histograms, sometimes too large.


print('''''')
print('''        Reweighting with GBReweighter''')
print('''''')

from hep_ml.reweight import GBReweighter


#The HEP ML algorithms want the data in an array, with one element per event, and that element is an array of features

# Let's build the inputs for the data
print('''Building the data training set''')
dataevents = []
for ievent in range(datatree.GetEntries()):
  datatree.GetEntry(ievent)
  dataevents.append([datatree.B_PT, datatree.B_ETA])


# and the inputs for the MC
print('''Building the MC training set''')
mcevents = []
for ievent in range(mctree.GetEntries()):
  mctree.GetEntry(ievent)
  mcevents.append([mctree.B_PT, mctree.B_ETA])

# Let's define our reweighting object, we can use the default settings for now
reweighter = GBReweighter()

# Next we train the ML algorithm to reproduce the data distribution from the MC distribution
# NB if the events are already weighted, add them here
print('''Training the reweighter''')
reweighter.fit(original=mcevents, target=dataevents)

# Next we run the algorithm on the MC to produce a weight for each of the events
print('''Applying the reweighter''')
mcweights = reweighter.predict_weights(mcevents)
# Before we apply the weights, let's make sure they are nicely normalised
Norm = len(mcweights)/sum(mcweights)
mcweights = [ Norm*el for el in mcweights]

# Now we have the weight for each MC event, so let's plot
print('''Building the MC histograms''')
hgbw = TH1D('', ';GB weight', 100, min(mcweights), max(mcweights))
hbpt_gbw = TH1D('',';B_{PT}', 100, 0, 20)
hbeta_gbw = TH1D('',';B #eta', 100, 0, 8)
hbptveta_gbw = TH2D('',';B_{PT};B #eta', 10, 0, 20, 10, 0, 8)
hkstpt_gbw = TH1D('',';K^{*0}_{PT}', 100, 0, 10)
hksteta_gbw = TH1D('',';K^{*0} #eta', 100, -2, 5)
for ievent in range(mctree.GetEntries()):
  mctree.GetEntry(ievent)
  w = mcweights[ievent]
  hgbw.Fill(w)
  hbpt_gbw.Fill(mctree.B_PT, w)
  hbeta_gbw.Fill(mctree.B_ETA, w)
  hbptveta_gbw.Fill(mctree.B_PT, mctree.B_ETA, w)
  hkstpt_gbw.Fill(mctree.Kst_PT, w)
  hksteta_gbw.Fill(mctree.Kst_ETA, w)

for h in [hbpt_gbw, hbeta_gbw, hkstpt_gbw, hksteta_gbw, hgbw]:
  h.SetLineColor(kBlue)
  h.SetLineWidth(2)

canvas_pt_gbw = TCanvas()
drawhists([hbpt_data, hbpt_gbw])

canvas_eta_gbw = TCanvas()
drawhists([hbeta_data, hbeta_gbw])

canvas_kstpt_gbw = TCanvas()
drawhists([hkstpt_data, hkstpt_gbw])

canvas_gbw = TCanvas()
hgbw.Draw()


print('''Some plots after using the GBReweighter''')
input("Press Enter to continue...")


###############################################################################
#                                                                             #
#        Using the HEP ML algorithm to do the reweighting, with folding       #
#                                                                             #
###############################################################################

# Sometimes we care about the statistical independence of the sets used to
# make weights and those to which we apply them. To achive this independence,
# we use k folding, where k classifiers are trained on a fraction (k-1)/k of
# the data. Only the classifier not trained on a particular event is used to
# determine the weight. Luckily this is all handled by the FoldingReweighter.
# The down side to folding is the reduction of the data being trained on.

print('''''')
print('''        Reweighting with FoldingReweighter''')
print('''''')


from hep_ml.reweight import FoldingReweighter

# We now have a new reweighting object, this defaults to k = 2 folds.
reweighter_base = GBReweighter()
reweighter_folded = FoldingReweighter(reweighter_base)

# The rest of the procedure is the sa,e
print('''Training the reweighter''')
reweighter_folded.fit(original=mcevents, target=dataevents)

#Care must be take to maintain the same order of events
print('''Applying the reweighter''')
mcweights_folded = reweighter_folded.predict_weights(mcevents)
Norm = len(mcweights_folded)/sum(mcweights_folded)
mcweights_folded = [ Norm*el for el in mcweights_folded]

# Now we have the weight for each MC event, so let's plot
print('''Building the MC histograms''')
hfw = TH1D('', ';Folded ML weight', 100, min(mcweights), max(mcweights))
hbpt_fw = TH1D('',';B_{PT}', 100, 0, 20)
hbeta_fw = TH1D('',';B #eta', 100, 0, 8)
hbptveta_fw = TH2D('',';B_{PT};B #eta', 10, 0, 20, 10, 0, 8)
hkstpt_fw = TH1D('',';K^{*0}_{PT}', 100, 0, 10)
hksteta_fw = TH1D('',';K^{*0} #eta', 100, -2, 5)
for ievent in range(mctree.GetEntries()):
  mctree.GetEntry(ievent)
  w = mcweights_folded[ievent]
  hfw.Fill(w)
  hbpt_fw.Fill(mctree.B_PT, w)
  hbeta_fw.Fill(mctree.B_ETA, w)
  hbptveta_fw.Fill(mctree.B_PT, mctree.B_ETA, w)
  hkstpt_fw.Fill(mctree.Kst_PT, w)
  hksteta_fw.Fill(mctree.Kst_ETA, w)

for h in [hbpt_fw, hbeta_fw, hkstpt_fw, hksteta_fw, hfw]:
  h.SetLineColor(kMagenta)
  h.SetLineWidth(2)

canvas_pt_gbw = TCanvas()
drawhists([hbpt_data, hbpt_fw])

canvas_eta_gbw = TCanvas()
drawhists([hbeta_data, hbeta_fw])

canvas_kstpt_gbw = TCanvas()
drawhists([hkstpt_data, hkstpt_fw])

canvas_gbw = TCanvas()
hfw.Draw()


print('''Some plots after using the Folded GBReweighter''')
input("Press Enter to continue...")

###############################################################################
#                                                                             #
#                       Comparing the different methods                       #
#                                                                             #
###############################################################################


# In this simple example all the weightings have similar performances, but this
# needs to be checked for a particular problem.
# Anyway, let's compare them all

print('''''')
print('''        Comparing all methods''')
print('''''')


canvas_pt_all = TCanvas()
drawhists([hbpt_data, hbpt_mc, hbpt_bw, hbpt_gbw, hbpt_fw])


canvas_eta_all = TCanvas()
drawhists([hbeta_data, hbeta_mc, hbeta_bw, hbeta_gbw, hbeta_fw])

canvas_kstpt_all = TCanvas()
drawhists([hkstpt_data, hkstpt_mc, hkstpt_bw, hkstpt_gbw, hkstpt_fw])

canvas_all = TCanvas()
drawhists([hbw, hgbw, hfw])
canvas_all.SetLogy(True)

print('''Some plots comparing all methods''')
input("Press Enter to Exit...")




