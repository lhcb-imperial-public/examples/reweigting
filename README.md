# Reweigting

This is a tutorial on how to reweight data in various ways. There can be many situations in which you wish to reweight data. For example, if there is a small mismodelling between data and simulation, or if you want to change the kinematics of one mode to look like another. This example will go through some of the techniques that can be used to reweight some 2D distributions using simple histograms, or machine learning.

## Running

  * source ./setup.sh
    * When first run, this will set up the python environment and download the required packages on the imperial machines. Subsequent calls will just activate the environment
  * python3 python/runme.py
  * deactivate
    * This will stop the python virtual environment

In the default configuration you'll find that all of the weighting methods do pretty much the same thing. Have a look at what happens when larger correlated mismodellings are included, an example data set is in data/example2.py. The histograms require many more bins to capture the behaviour.
